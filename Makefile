# Build path
BUILD_DIR = build

FILES = \
	CV-GarciaTroya-EN.tex

.PHONY: all
all: $(patsubst %.tex,%.pdf,$(FILES))

%.pdf: %.tex $(BUILD_DIR) | Makefile
	-pdflatex --output-directory=$(BUILD_DIR) --interaction=nonstopmode $^

$(BUILD_DIR):
	mkdir $@

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) 
